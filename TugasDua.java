import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.security.SecureRandom;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class TugasDua extends JPanel
{
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        SecureRandom random = new SecureRandom();

        int[] x = {55,67,109,73,83,55,27,37,1,43};
        int[] y = {0,36,36,54,96,72,96,54,36,36};

        Graphics2D g2 = (Graphics2D) g;
        GeneralPath bintang = new GeneralPath();

        bintang.moveTo(x[0], y[0]);

        for(int i=1;i<x.length;i++){
            bintang.lineTo(x[i], y[i]);
        }

        bintang.closePath();

        g2.translate(150, 150);

        for(int i =1;i<=20;i++){
            g2.rotate(Math.PI/10.0);

            g2.setColor(new Color(random.nextInt(256),random.nextInt(256),random.nextInt(256)));
            g2.fill(bintang);
            
        }
    }

    public static void main(String[] args){
        JFrame frame = new JFrame("Drawing 2D Shapes");
        JLabel label = new JLabel();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        TugasDua coba = new TugasDua();
        frame.add(coba,BorderLayout.CENTER);
        frame.add(label,BorderLayout.NORTH);
        frame.setSize(315,330);
        frame.setVisible(true);
    }
}
