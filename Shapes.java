import javax.swing.JFrame;

public class Shapes {
	public static void main(String []args) {
		JFrame frame = new JFrame("Drawing 2d Shapes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ShapessJPanel ShapesJPanel = new ShapessJPanel();
		frame.add( ShapesJPanel);
		frame.setSize(425, 200);
		frame.setVisible(true);
	}

}
