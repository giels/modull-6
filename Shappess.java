import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Shappess extends JPanel{
	int xPoints[] = new int[] {300, 420, 360};
	int yPoints[] = new int[] {120, 120, 20};
public Shappess(){
	setLayout(null);
	setSize(600,180);
}
public void paint(Graphics g) {
	g.fillArc(20, 20, 100, 100, 180, 180);
	g.fillOval(150, 20, 120, 100);
	g.fillPolygon(xPoints, yPoints, 3);
	g.fillRect(450, 20, 100, 100);
}
	public static void main(String[] args) {
		JFrame frame = new JFrame("Shappes");
		Shappess shapess = new Shappess();
		
		frame.setLayout(null);
		frame.add(shapess);
		frame.setSize(600, 180);
		frame.setVisible(true);
	}

}
