import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Double;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class shapess2 extends JPanel{
	
	final static float dash1[] = {10.0f};
    final static BasicStroke dashed =
        new BasicStroke(1.0f,
                        BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER,
                        10.0f, dash1, 0.0f);
	final static BasicStroke wideStroke = new BasicStroke(8.0f);
    
	public shapess2() {
		setLayout(null);
		setSize(600,180);
	}
	
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.draw(new Line2D.Double(20, 120, 130, 20));
		g2.draw(new Rectangle2D.Double(150, 20, 130, 100));
		g2.setStroke(dashed);
		g2.draw(new RoundRectangle2D.Double(300, 20, 130, 100, 10, 10));
		g2.setStroke(wideStroke);
		g2.draw(new Arc2D.Double(450, 50, 130, 50, 90, 135, Arc2D.OPEN));
		
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Shapess");
		shapess2 shapess = new shapess2();
		
		frame.setLayout(null);
		frame.add(shapess);
		
		frame.setSize(600,180);
		frame.setVisible(true);
	}

}
